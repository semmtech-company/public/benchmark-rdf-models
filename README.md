# Benchmark RDF Modes

## Collection of RDF Models which can be used to investigate performance

### RDF Models

The following datasets are currently available. All these resource are available as N-Triples.

| Dataset | Description | Size |
|---|---|---|
| Huge | dataset based on a DBpedia project called "geo_coordinates_en.nt" in N-Triples format, enriched with an rdfs:comment property on all 300.000 subjects containing lorem ipsum text with 8 to 20 words. | 1.5M |
| Large | dataset containing 1M triples (same as large, but only containing the first 1,000,000 triples) | 1M |
| Normal | dataset containing 640k triples (same as large, but only containing the first 640,000 triples) | 640k |
| Medium | dataset containing 250k triples (same as large, but only containing the first 250,000 triples) | 250k |
| Small | dataset containing 50k triples (same as large, but only containing the first 50,000 triples) | 50k |
| Tiny | dataset containing 7.5k triples (same as large, but only containing the first 7,500 triples) | 7.5k |

## SPARQL Queries

We should test various SPARQL aspects, e.g. UNION, REGEX.
We can create a set of 5 or more standrd queries to use for testing performance of various dataset sizes.

Mohamed Morsey can help with that.
